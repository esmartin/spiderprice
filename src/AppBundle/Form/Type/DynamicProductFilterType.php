<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\Choice\CategoryChoiceType;
use AppBundle\Form\Type\Choice\WebsiteChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Dynamic product Filter Type.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
class DynamicProductFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'empty_data' => null,
                'required' => false,
                'label' => 'label.product_filter.name',
            ])
            ->add('category', CategoryChoiceType::class, [
                'placeholder' => 'label.product_filter.category'
            ])
            ->add('website', WebsiteChoiceType::class, [
                'placeholder' => 'label.product_filter.website',
            ]);
    }
}