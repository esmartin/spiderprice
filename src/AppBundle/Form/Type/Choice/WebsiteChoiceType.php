<?php

namespace AppBundle\Form\Type\Choice;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Website Choice Type
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
class WebsiteChoiceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => 'AppBundle:Website',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('w')->orderBy('w.name', 'ASC');
            },
            'placeholder' => '',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return EntityType::class;
    }
}