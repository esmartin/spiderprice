<?php

namespace AppBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * Product.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Doctrine\ORM\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Product
{
    use Timestampable;

    /**
     * @var mixed
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @var Website
     *
     * @ORM\ManyToOne(targetEntity="Website")
     */
    private $website;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $img;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Price", mappedBy="product", cascade="all")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $prices;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $pricesValues = [];

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Feature", mappedBy="product", cascade="all")
     */
    private $features;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=2)
     */
    private $lastPrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=2)
     */
    private $previousPrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=2)
     */
    private $percentageLastPrice;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $disableCounter;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->lastPrice = 0;
        $this->previousPrice = 0;
        $this->percentageLastPrice = 0;
        $this->disableCounter = 0;
        $this->pricesValues = [];
        $this->prices = new ArrayCollection();
        $this->features = new ArrayCollection();
    }

    /**
     * Returns the id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name.
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the website.
     *
     * @return Website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets the website.
     *
     * @param Website $website
     */
    public function setWebsite(Website $website)
    {
        $this->website = $website;
    }

    /**
     * Returns the category.
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the category.
     *
     * @param Category $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Returns the url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets the url.
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Returns the img.
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Sets the img.
     *
     * @param string $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * Returns the prices.
     *
     * @return ArrayCollection
     */
    public function getPrices()
    {
        return $this->prices;
    }


    /**
     * Adds a price.
     *
     * @param Price $price
     */
    public function addPrice(Price $price)
    {
        $this->prices->add($price);
    }

    /**
     * Removes a price.
     *
     * @param Price $price
     */
    public function removePrice(Price $price)
    {
        $this->prices->removeElement($price);
    }

    /**
     * Return prices values
     *
     * @return array
     */
    public function getPricesValues()
    {
        return $this->pricesValues;
    }

    /**
     * Sets the prices values
     *
     * @param array $pricesValues
     */
    public function setPricesValues($pricesValues)
    {
        $this->pricesValues = $pricesValues;
    }

    /**
     * Adds a price value.
     *
     * @param float $priceValue
     */
    public function addPricesValue($priceValue)
    {
        $this->pricesValues[] = $priceValue;
    }

    /**
     * Returns the features.
     *
     * @return ArrayCollection
     */
    public function getFeatures()
    {
        return $this->features;
    }

     /**
     * Adds a feature.
     *
     * @param Feature $feature
     */
    public function addFeature(Feature $feature)
    {
        $this->features->add($feature);
    }

    /**
     * Removes a feature.
     *
     * @param Feature $feature
     */
    public function removeFeature(Feature $feature)
    {
        $this->features->removeElement($feature);
    }

    /**
     * Returns the last price.
     *
     * @return float
     */
    public function getLastPrice()
    {
        return $this->lastPrice;
    }

    /**
     * Sets the last price.
     *
     * @param float $lastPrice
     */
    public function setLastPrice($lastPrice)
    {
        $this->lastPrice = $lastPrice;
    }

    /**
     * Returns the previous price.
     *
     * @return float
     */
    public function getPreviousPrice()
    {
        return $this->previousPrice;
    }

    /**
     * Sets the previous price.
     *
     * @param float $previousPrice
     */
    public function setPreviousPrice($previousPrice)
    {
        $this->previousPrice = $previousPrice;
    }

    /**
     * Returns the percentage last price.
     *
     * @return float
     */
    public function getPercentageLastPrice()
    {
        return $this->percentageLastPrice;
    }

    /**
     * Sets the percentage last price.
     *
     * @param float $percentageLastPrice
     */
    public function setPercentageLastPrice($percentageLastPrice)
    {
        $this->percentageLastPrice = $percentageLastPrice;
    }

    /**
     * Updates the last price
     */
    public function updateLastPrice()
    {
        return $this->getPricesValues()[count($this->getPricesValues()) - 1];
    }

    /**
     * Updates the previous price
     */
    public function updatePreviousPrice()
    {
        return (count($this->getPricesValues()) > 1 ? $this->getPricesValues()[count($this->getPricesValues()) - 2] : $this->getLastPrice());
    }

    /**
     * Updates the percentage last price
     */
    public function updatePercentageLastPrice()
    {
        $percentage = 0;
        if (count($this->getPricesValues()) >= 2) {
            $lastPrice = $this->getLastPrice();
            $lastPrice = $lastPrice <= 0 ? 1 : $lastPrice;
            $initPrice = $this->getPreviousPrice();
            $initPrice = $initPrice <= 0 ? 1 : $initPrice;
            $percentage = (($lastPrice / $initPrice) * 100) - 100;
        }

        return $percentage;
    }

    /**
     * Returns the disable counter.
     *
     * @return int
     */
    public function getDisableCounter()
    {
        return $this->disableCounter;
    }

    /**
     * Sets the disable counter.
     *
     * @param int $disableCounter
     */
    public function setDisableCounter($disableCounter)
    {
        $this->disableCounter = $disableCounter;
    }

    /**
     * Returns the product name.
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }
}