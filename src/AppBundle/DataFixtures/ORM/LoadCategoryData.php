<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Model\Category;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load Category Fixtures.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
class LoadCategoryData extends LoadDataFixtures implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $categories = ['smartphone', 'television', 'camera'];

        foreach ($categories as $categoryLoad) {
            $category = new Category();
            $category->setName($categoryLoad);
            $manager->persist($category);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
