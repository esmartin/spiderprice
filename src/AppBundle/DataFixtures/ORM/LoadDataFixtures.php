<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load Data Fixtures.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
abstract class LoadDataFixtures extends ContainerBuilder implements FixtureInterface
{
    /**
     * Loads a file content.
     *
     * @param string $filename
     *
     * @return string
     */
    public function loadFileContent($filename)
    {
        $path = '/var/www/dynamic-price/app/Resources/data-fixtures/'.$filename;

        return file_get_contents($path);
    }
}