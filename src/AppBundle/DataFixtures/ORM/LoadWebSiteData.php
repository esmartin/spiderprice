<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Model\Website;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load Website Fixtures.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
class LoadWebSiteData extends LoadDataFixtures implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $websites = [
            'amazon'       => 'Amazon',
            'media-markt'  => 'Media Markt',
            'phone-house'  => 'Phone House',
            'eci'          => 'El Corte Ingles',
            'fnac'         => 'Fnac'
        ];

        foreach ($websites as $code => $name) {
            $website = new Website();
            $website->setCode($code);
            $website->setName($name);

            $manager->persist($website);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
