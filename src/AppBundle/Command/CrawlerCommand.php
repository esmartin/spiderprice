<?php

namespace AppBundle\Command;

use AppBundle\Model\Category;
use AppBundle\Model\Product;
use AppBundle\Model\Price;
use AppBundle\Model\Feature;
use AppBundle\Model\Website;
use Goutte\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * Crawler command.
 *
 * @author Esteban Martin <esteban2493@gmail.com>
 */
class CrawlerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('app:crawler');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $products = $em->getRepository('AppBundle:Product')
                        ->createQueryBuilder('p')
                        ->select('p, w')
                        ->innerJoin('p.website','w')
                        ->orderBy('p.category','DESC')
                        ->getQuery()->getResult();

        $total = count($products);

        $client = new Client();

        $i = 1;

        foreach ($products as $product) {

            $disableCounter = $product->getDisableCounter();

            if ($disableCounter > 2) {
                $product->setImg('notavailable.gif');
                $em->persist($product);
                continue;
            }

            $entityProduct = null;
            $website = $product->getWebsite();

            $entityProduct = $this->updateProduct($product, $website, $em, $client, $output);

            if ($entityProduct === null) {
                $disableCounter++;
                $product->setDisableCounter($disableCounter);
                $em->persist($product);
                $em->flush();
                gc_collect_cycles();
                $output->writeln('<error>['. $i .'/'. $total .'] - Product name: '. $product->getName() .', website : '. $product->getWebsite()->getName().'</error>');
                $i++;
                continue;
            }

            $output->writeln('['. $i .'/'. $total .'] - Product name: '. $entityProduct->getName() .', website : '. $entityProduct->getWebsite()->getName());

            $em->persist($entityProduct);

            if ($i % 100 == 0) {
                $em->flush();
                gc_collect_cycles();
            }

            $i++;
        }

        $em->flush();
    }

    /**
     * {@inheritdoc}
     */
    protected function updateProduct(Product $product, Website $website, $manager, Client $client, $output)
    {
        $productUpdated = $features = $data = null;

        switch ($website->getName()) {
            case 'Amazon':
                $productUpdated = $this->updateAmazon($product, $client, $output);
                break;
            case 'Media Markt':
                $productUpdated = $this->updateMediaMarkt($product, $client, $output);
                break;
            case 'Phone House':
                $productUpdated = $this->updatePhoneHouse($product, $client, $output);
                break;
            case 'El Corte Ingles':
                $productUpdated = $this->updateECI($product, $client, $output);
                break;
            case 'Fnac':
                $productUpdated = $this->updateFnac($product, $client, $output);
                break;
            default:
                break;
        }

        if ($productUpdated === '404') {
            $productUpdated = null;
        }

        return $productUpdated;
    }

    /**
     * {@inheritdoc}
     */
    protected function updateAmazon(Product $product, Client $client, $output)
    {
        $html = $client->request('GET', $product->getUrl());
        $priceNode = $price = null;

        if($client->getResponse()->getStatus() == 404){
            return '404';
        }

        $priceNode = $html->filter('#priceblock_ourprice');

        if (count($priceNode)) {

            $priceString = str_replace("EUR ", "", $priceNode->text());
            $value = floatval(str_replace(',', '.', str_replace('.', '', $priceString)));

            $price = $this->createPrice($value, $product);

        }else{

            $price = $this->createPrice(-1, $product);
        }

        if (count($product->getPricesValues()) == 0) {
            foreach ($product->getPrices() as $price) {
                $product->addPricesValue($price->getValue());
            }
        }else{

            $product->addPrice($price);
            $product->addPricesValue($price->getValue());
        }

        if (count($product->getPricesValues()) <= 1) {
            $product->addPricesValue(0);
            $product->setLastPrice(0);
            $product->setPreviousPrice(0);

        }else{

            $product->setLastPrice($product->updateLastPrice());
            $product->setPreviousPrice($product->updatePreviousPrice());

        }

        $product->setLastPrice($product->updateLastPrice());
        $product->setPreviousPrice($product->updatePreviousPrice());

        if ($product->getLastPrice() != $product->getPreviousPrice()) {
            $product->setPercentageLastPrice($product->updatePercentageLastPrice());
        }else{
            $product->setPercentageLastPrice(0);
        }

        $imageNode = $html->filter('#landingImage');

        if(count($imageNode)){

            $url = json_decode($imageNode->attr('data-a-dynamic-image'), true);

            $imageUrl = array_keys($url)[0];

            $image = $this->downloadImage($product, $imageUrl, $client, $output);

            if ($image !== null) {
                $product->setImg($image);
            }

        }else{

            $product->setImg('notavailable.gif');
        }


        return $product;
    }

    /**
     * {@inheritdoc}
     */
    protected function updateMediaMarkt(Product $product, Client $client, $output)
    {
        $html = $client->request('GET', $product->getUrl());
        $priceNode = $price = null;

        if($client->getResponse()->getStatus() == 404){
            return '404';
        }

        $priceNode = $html->filter('.mm-price');

        if (count($priceNode)){

            $value = floatval($priceNode->attr('price'));

            $price = $this->createPrice($value, $product);

        }else{

            $price = $this->createPrice(-1, $product);
        }

        if (count($product->getPricesValues()) == 0) {
            foreach ($product->getPrices() as $price) {
                $product->addPricesValue($price->getValue());
            }
        }else{

            $product->addPrice($price);
            $product->addPricesValue($price->getValue());
        }

        if (count($product->getPricesValues()) <= 1) {
            $product->addPricesValue(0);
            $product->setLastPrice(0);
            $product->setPreviousPrice(0);

        }else{

            $product->setLastPrice($product->updateLastPrice());
            $product->setPreviousPrice($product->updatePreviousPrice());

        }

        $product->setLastPrice($product->updateLastPrice());
        $product->setPreviousPrice($product->updatePreviousPrice());

        if ($product->getLastPrice() != $product->getPreviousPrice()) {
            $product->setPercentageLastPrice($product->updatePercentageLastPrice());
        }else{
            $product->setPercentageLastPrice(0);
        }

        $imageNode = $html->filter('div.productDetailImage img');

        if(count($imageNode)){
            $imageUrl = $imageNode->attr('src');

            $image = $this->downloadImage($product, $imageUrl, $client, $output);

            if ($image !== null) {
                $product->setImg($image);
            }

        }else{

            $product->setImg('notavailable.gif');
        }


        return $product;
    }

    /**
     * {@inheritdoc}
     */
    protected function updatePhoneHouse(Product $product, Client $client, $output)
    {
        $html = $client->request('GET', $product->getUrl());
        $priceNode = $price = null;

        if($client->getResponse()->getStatus() == 404){
            return '404';
        }

        $priceNode = count($html->filter('div#libre p')) > 0 ? $html->filter('div#libre p') : $html->filter('.caja-precio strong');

        if (count($priceNode)){

            $priceString = trim(str_replace("€", "", $priceNode->text()));
            $value = floatval($priceString);

            $price = $this->createPrice($value, $product);

        }else{

            $price = $this->createPrice(-1, $product);
        }

        if (count($product->getPricesValues()) == 0) {
            foreach ($product->getPrices() as $price) {
                $product->addPricesValue($price->getValue());
            }
        }else{

            $product->addPrice($price);
            $product->addPricesValue($price->getValue());
        }

        if (count($product->getPricesValues()) <= 1) {
            $product->addPricesValue(0);
            $product->setLastPrice(0);
            $product->setPreviousPrice(0);

        }else{

            $product->setLastPrice($product->updateLastPrice());
            $product->setPreviousPrice($product->updatePreviousPrice());

        }

        $product->setLastPrice($product->updateLastPrice());
        $product->setPreviousPrice($product->updatePreviousPrice());

        if ($product->getLastPrice() != $product->getPreviousPrice()) {
            $product->setPercentageLastPrice($product->updatePercentageLastPrice());
        }else{
            $product->setPercentageLastPrice(0);
        }

        $imageNode = count($html->filter('img.activo')) > 0 ? $html->filter('img.activo') : $html->filter('.ficha div div img');

        if(count($imageNode)){

            $imageUrl = $imageNode->attr('src');

            $image = $this->downloadImage($product, "http:".$imageUrl, $client, $output);

            if ($image !== null) {
                $product->setImg($image);
            }

        }else{

            $product->setImg('notavailable.gif');
        }

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    protected function updateECI(Product $product, Client $client, $output)
    {
        $html = $client->request('GET', $product->getUrl());
        $priceNode = $price = $image = null;

        if($client->getResponse()->getStatus() == 404){
            return '404';
        }

        $priceNode = $html->filter('div.product-price span.current');

        if (count($priceNode)){

            $priceString = trim(str_replace("€", "", $priceNode->text()));
            $value = floatval(str_replace(',', '.', str_replace('.', '', $priceString)));

            $price = $this->createPrice($value, $product);

        }else{

            $price = $this->createPrice(-1, $product);
        }

        if (count($product->getPricesValues()) == 0) {
            foreach ($product->getPrices() as $price) {
                $product->addPricesValue($price->getValue());
            }
        }else{

            $product->addPrice($price);
            $product->addPricesValue($price->getValue());
        }

        if (count($product->getPricesValues()) <= 1) {
            $product->addPricesValue(0);
            $product->setLastPrice(0);
            $product->setPreviousPrice(0);

        }else{

            $product->setLastPrice($product->updateLastPrice());
            $product->setPreviousPrice($product->updatePreviousPrice());

        }

        $product->setLastPrice($product->updateLastPrice());
        $product->setPreviousPrice($product->updatePreviousPrice());

        if ($product->getLastPrice() != $product->getPreviousPrice()) {
            $product->setPercentageLastPrice($product->updatePercentageLastPrice());
        }else{
            $product->setPercentageLastPrice(0);
        }

        $imageNode = $html->filter('img#product-image-placer');

        if(count($imageNode)){

            $imageUrl = $imageNode->attr('src');

            $image = $this->downloadImage($product, "http:".$imageUrl, $client, $output);

            if ($image !== null) {
                $product->setImg($image);
            }

        }else{

            $product->setImg('notavailable.gif');
        }


        return $product;
    }

    /**
     * {@inheritdoc}
     */
    protected function updateFnac(Product $product, Client $client, $output)
    {
        $html = $client->request('GET', $product->getUrl());
        $priceNode = $price = null;

        if($client->getResponse()->getStatus() == 404){
            return '404';
        }

        $priceNode = $html->filter('.price');

        if (count($priceNode)){

            $value = floatval(str_replace(',', '.', str_replace('.', '', trim(str_replace("€", "", $priceNode->text())))));
            $price = $this->createPrice($value, $product);

        }else{

            $price = $this->createPrice(-1, $product);
        }

        if (count($product->getPricesValues()) == 0) {
            foreach ($product->getPrices() as $price) {
                $product->addPricesValue($price->getValue());
            }
        }else{

            $product->addPrice($price);
            $product->addPricesValue($price->getValue());
        }

        if (count($product->getPricesValues()) <= 1) {
            $product->addPricesValue(0);
            $product->setLastPrice(0);
            $product->setPreviousPrice(0);

        }else{

            $product->setLastPrice($product->updateLastPrice());
            $product->setPreviousPrice($product->updatePreviousPrice());

        }

        $product->setLastPrice($product->updateLastPrice());
        $product->setPreviousPrice($product->updatePreviousPrice());

        if ($product->getLastPrice() != $product->getPreviousPrice()) {
            $product->setPercentageLastPrice($product->updatePercentageLastPrice());
        }else{
            $product->setPercentageLastPrice(0);
        }

        $imageNode = $html->filter('.nav-diaporama img');

        if(count($imageNode)){

            $imageUrl = $imageNode->attr('src');

            $image = $this->downloadImage($product, $imageUrl, $client, $output);

            if ($image !== null) {
                $product->setImg($image);
            }

        }else{

            $product->setImg('notavailable.gif');
        }

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    protected function createPrice($value, Product $product)
    {
        $price = new Price();
        $price->setValue($value);
        $price->setProduct($product);

        return $price;
    }

    /**
     * {@inheritdoc}
     */
    protected function downloadImage(Product $product, $imageUrl, Client $client, $output)
    {
        $imageName = $product->getImg();

        if ($imageName == "" || $imageName == 'notavailable.gif') {

            $ext = pathinfo($imageUrl, PATHINFO_EXTENSION);

            $hash = md5(uniqid());

            $imageName = $hash.'.'.$ext;

            $path = $this->getContainer()->getParameter('kernel.root_dir').'/../web/uploads/'.$imageName;

            $product->setImg($imageName);

            $client->request('GET', $imageUrl);
            $file = $client->getResponse()->getContent();

            file_put_contents($path, $file);

            return $imageName;

        }

        $path = $this->getContainer()->getParameter('kernel.root_dir').'/../web/uploads/'.$imageName;

        if (!file_exists($path)) {

            $product->setImg($imageName);

            $client->request('GET', $imageUrl);
            $file = $client->getResponse()->getContent();

            file_put_contents($path, $file);

            return $imageName;
        }

        return null;
    }
}