<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Security Controller.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
class SecurityController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/login", name="security_login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', [
            'lastUsername' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Method("POST")
     * @Route("/login_check", name="security_check")
     */
    public function checkAction()
    {
    }

    /**
     * @Method("GET")
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
    }
}