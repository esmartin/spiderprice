<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Base Controller.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
class BaseController extends Controller
{

    /**
     * Persists an entity.
     *
     * @param mixed $entity
     */
    public function persistEntity($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     * Removes an entity.
     *
     * @param mixed $entity
     */
    public function removeEntity($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
    }

    /**
    * Returns the current action name.
    *
    * @return string
    */
    public function getActionName()
    {
        $matches = [];
        preg_match('#::([a-zA-Z]*)Action#', $this->get('request_stack')->getCurrentRequest()->get('_controller'), $matches);

        return $matches[1];
    }

    public function processSeriesSpline($products)
    {
        $series = [];
        foreach ($products as $product) {
            $data = [];
            foreach ($product->getPrices() as $price) {
                $data [] = [$price->getCreatedAt()->getTimeStamp() * 1000, $price->getValue()];
            }
            $series[] = ["name" => $product->getWebsite()->getName()." - ".preg_replace("/[^a-zA-Z0-9 ]/", "", $product->getName()), "data" => $data];
        }

        return $series;
    }
    
    public function processSeriesPie($products)
    {
        $data = [];
        $cameras = $televisions = $smartphones = 0;
        foreach ($products as $key => $value) {
            $data[] = ["name" => $key, "y" => $value];
        }

        $series = [
            "name" => "Productos",
            "colorByPoint" => true,
            "data" => $data
        ];
        return [$series];
    }

    public function findProductsGroupped($products, $company = [])
    {
        $productsGroupped = [];

        foreach ($products as $product) {
            $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
            $companyProducts = count($company) > 0 ? $repository->findBy(["website" => $company, "matchedId" => $product->getMatchedId()]) : $repository->findBy(["matchedId" => $product->getMatchedId()]);

            $productsGroupped [] = ["product" => $product, "products" => $companyProducts];
        }

        return $productsGroupped;
    }

    public function calculateProductSerie($product, $company, $refWebsite)
    {
        $companyProduct = $this->getDoctrine()->getRepository('AppBundle:Product')->findOneBy(["matchedId" => $product->getMatchedId(), "website" => $company]);
        $refProduct = $this->getDoctrine()->getRepository('AppBundle:Product')->findOneBy(["name" => $product->getName(), "matchedId" => $product->getMatchedId(), "website" => $refWebsite]);
        $productReturned = [];

        if(($companyProduct !== null) && ($refProduct !== null)){

            if($refProduct->getLastPrice() > $companyProduct->getLastPrice()){
                $productReturned = ["catalogProduct0" => $product,"product" => $companyProduct];
            }elseif ($refProduct->getLastPrice() == $companyProduct->getLastPrice()) {
                $productReturned = ["catalogProduct1" => $product,"product" => $companyProduct];
            }else{
                $productReturned = ["catalogProduct2" => $product,"product" => $companyProduct];
            }
        }

        return $productReturned;
    }

    public function processGraphicData($productsGroupped, $companiesRaw = [], $refWebsite)
    {
        $series = $competitorsData = [];
        $companies = count($companiesRaw) == 0 ? $this->getDoctrine()->getRepository('AppBundle:Website')->findAll() : $companiesRaw;
        foreach ($companies as $company) {

            if ($company == $refWebsite) {
                continue;
            }

            $numberPlus = 0;
            $numberEqual = 0;
            $numberMinus = 0;
            $dataPlus = [];
            $dataEqual = [];
            $dataMinus = [];
            foreach ($productsGroupped as $product) {
                $countData = $this->calculateProductSerie($product['product'], $company, $refWebsite);

                if (array_key_exists('catalogProduct0', $countData)){
                    $dataPlus[] = $countData;
                    $numberPlus++;
                }elseif(array_key_exists('catalogProduct1', $countData)){
                    $dataEqual[] = $countData;
                    $numberEqual++;
                }elseif(array_key_exists('catalogProduct2', $countData)){
                    $dataMinus[] = $countData;
                    $numberMinus++;
                }
            }

            if ($numberMinus == 0 && $numberEqual == 0 && $numberPlus == 0){
                continue;
            }

            $competitorsData[$company->getName()] = [
                        "y0" => $numberPlus,
                        "y1" => $numberEqual,
                        "y2" => $numberMinus,
                        "drilldownData0" => $dataPlus,
                        "drilldownData1" => $dataEqual,
                        "drilldownData2" => $dataMinus,
                    ];
        }

        return $competitorsData;
    }

    public function processSeries($dataChart)
    {
        $series = [];
        $i = 0;
        while ($i < 3) {
            $data = [];
            foreach ($dataChart as $key => $result) {
                if ($result['y'.$i] == 0) {
                    $data[] = [
                        'name' => $key,
                        'y' => $result['y'.$i],
                        'drilldown' => null
                    ];
                }else{
                    $data[] = [
                        'name' => $key,
                        'y' => $result['y'.$i],
                        'drilldown' => trim($key.$i)
                    ];
                }
            }
            if ($i == 0) {
                $series[] = [
                    'enabledLabels' => false,
                    'name' => 'Precio Mayor',
                    'data' => $data,
                    'color' => '#4caf50'
                ];
            }elseif ($i == 1){
                $series[] = [
                    'enabledLabels' => false,
                    'name' => 'Precio Igual',
                    'data' => $data,
                    'color' => '#757575'
                ];
            }elseif ($i == 2){
                $series[] = [
                    'enabledLabels' => false,
                    'name' => 'Precio Menor',
                    'data' => $data,
                    'color' => '#ef5350'
                ];
            }
            $i++;
        }

        return $series;
    }

    public function processSeriesDrillDown($dataChart, $companiesRaw)
    {
        $series = [];
        foreach ($dataChart as $key => $value) {
            for ($i=0; $i < 3; $i++) {
                $data = [];
                foreach ($value['drilldownData'.$i] as $product) {
                    $name = preg_replace("/[^a-zA-Z0-9 ]/", "", $product['catalogProduct'.$i]->getName());
                    $data [] = [$name,floatval($product['product']->getLastPrice())];
                }
                if (count($data) == 0) {
                    continue;
                }

                $series[] = [
                    "id" => trim($key.$i),
                    "name" => $key,
                    "data" =>  $data,
                ];
            }
        }
        return $series;
    }
}