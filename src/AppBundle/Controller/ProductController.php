<?php

namespace AppBundle\Controller;

use AppBundle\Model\Product;
use AppBundle\Form\Type\DynamicProductFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Product Controller.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 *
 * @Route("/product")
 */
class ProductController extends BaseController
{
    /**
     * @Route("/list.{_format}", defaults={"_format": "html"}, requirements={"_format": "html|csv"}, name="product_list")
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository('AppBundle:Product');

        $products = $pagination = $series = [];
        $format = $request->getRequestFormat();
        $limit = $format === 'csv' ? 200 : 25;

        $form = $this->createForm(DynamicProductFilterType::class, [],['method' => 'GET']);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $products = $productRepo->getProductsDynamic($form->getData());

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate($products, $request->query->getInt('page', 1), $limit);

            $series = $this->processSeriesSpline($pagination);
        }

        return $this->render('product/list.'.$format.'.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'series' => str_replace("\"", "'", json_encode($series))
        ]);
    }

    /**
     * @Route("/{id}/show.{_format}", defaults={"_format": "html"}, requirements={"_format": "html|csv"}, name="product_show")
     * @ParamConverter("product", class="AppBundle:Product", options={"id" = "id"})
     */
    public function showAction(Request $request, Product $product)
    {
        $format = $request->getRequestFormat();
        $series = $this->processSeriesSpline([$product]);

        return $this->render('product/show.'.$format.'.twig', [
            'product' => $product,
            'series' => str_replace("\"", "'", json_encode($series))
        ]);
    }
}