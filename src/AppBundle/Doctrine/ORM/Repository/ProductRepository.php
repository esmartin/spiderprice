<?php

namespace AppBundle\Doctrine\ORM\Repository;

use AppBundle\Model\Configuration;
use AppBundle\Model\Website;
use Doctrine\ORM\EntityRepository;

/**
 * Product Repository.
 *
 * @author Esteban Martín <esteban2493@gmail.com>
 */
class ProductRepository extends EntityRepository
{
    /**
     * find products options given
     *
     * @var filters[]
     *
     * @return []
     */
    public function getProductsDynamic($filters = [])
    {
        $qb = $this->createQueryBuilder('p')
                    ->select('p, c, w, pr')
                    ->innerJoin('p.website', 'w')
                    ->innerJoin('p.category', 'c')
                    ->innerJoin('p.prices', 'pr');

        if (array_key_exists('name', $filters) && $filters['name'] !== null) {
            $qb->where("p.name LIKE :name");
        }

        if (array_key_exists('website', $filters) && count($filters['website'])) {
            $qb->andWhere('w.id IN (:website)');
        }

        if (array_key_exists('category', $filters) && count($filters['category'])) {
            $qb->andWhere('c.id in (:category)');
        }

        if (array_key_exists('name', $filters) && $filters['name'] !== null) {
            $qb->setParameter('name', '%'.$filters['name'].'%');
        }

        if (array_key_exists('website', $filters) && count($filters['website'])) {
            $qb->setParameter('website', $filters['website']);
        }

        if (array_key_exists('category', $filters) && count($filters['category'])) {
            $qb->setParameter('category', $filters['category']);
        }


        return $qb;
    }
}