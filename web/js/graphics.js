function graphics(refCompany, series, drilldownSeries){

    // Create the chart
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Competencia respecto a '+refCompany
        },
        xAxis: {
            type: 'category',
        },

        yAxis: {
             min: 0,
        },

        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    style: {
                        color: 'white',
                        textShadow: '0 0 2px black, 0 0 2px black'
                    }
                },
                stacking: 'normal'
            }
        },
        series: series,
        drilldown: {
            activeDataLabelStyle: {
                color: 'white',
                textShadow: '0 0 2px black, 0 0 2px black'
            },
            series: drilldownSeries
        }
    })
}

function basicColumn(series, title){

    // Create the chart
    $('#columnChart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Precio ' + title
        },
        xAxis: {
            type: 'category'
        },

        yAxis: {
            min: 0,
        },

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        series: series
    })
}

function splineGraphic(series){

    // Create the chart
    $('#chart').highcharts({
        legend: {
            enabled: false
        },
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Lista de precios'
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            labels: {
                format: '{value} €'
            },
            min: 0,
        },
        series: series
    })
};

function pieGraphic(series){

    $('#chart').highcharts({
        chart: {
            type: 'pie'
        },

        title: {
            text: 'Numero de productos'
        },

        series: series
    })
};